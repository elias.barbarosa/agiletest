import model.Person;
import model.Transaction;
import repositories.TransactionRepository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Logger;

public class CSVReportService {

    private final PersonsService personsService;
    private final TransactionRepository transactionRepository;

    public CSVReportService(PersonsService personsService, TransactionRepository transactionRepository) {
        this.personsService = personsService;
        this.transactionRepository = transactionRepository;
    }

    /**
     * Retrieve the average consumption (transaction amount) per @{@link model.Person}'s distinct roles during the last month
     * <p>
     * Note that roles are just tags that each person is assigned. ie 'student', 'gamer', 'athlete', 'parent'
     * a Person may have multiple roles or none.
     *
     * @return data in csv file format,
     * where the first line depict the roles
     * and the second line the average consumption per role
     * ie: (formatted example -- the actual output should be just comma separated)
     * |student, gamer, parent|
     * |10.50  , 20.10, 0     |
     */
    public String getAverageConsumptionPerRoleDuringTheLastMonth() {

        Map<String, Double> csvMap = new HashMap<>();

        transactionRepository.getTransactions().stream().forEach(t -> {

            personsService.getPersonByEmailAddress(t.getEmailAddress()).get().getRoles().stream().forEach(role -> {
                csvMap.put(role, transactionRepository.getTransactions().stream()
                        .filter(transaction -> transaction.getEmailAddress() == t.getEmailAddress())
                        .mapToLong(Transaction::getAmount).average().getAsDouble());
            });


        });

        StringBuilder csv = new StringBuilder("");
        csvMap.entrySet().stream().forEach(e-> csv.append(e.getKey()+","));
        csv.append("\n");
        csvMap.entrySet().stream().forEach(e->csv.append(e.getValue()+","));
        return csv.toString();
    }
}
